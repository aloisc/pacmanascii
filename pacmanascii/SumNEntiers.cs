using System;

namespace pacmanascii {

	class SumNEntiers {

		private int n { get; set;}

		public int N {get; set;}

		private int sum() {

			return 1;
		}

	}

	class TestSumNEntiers : AbstractTest {

		public TestSumNEntiers (string name) : base(name) {}

		private int oracle (int n) {

			return n;
		}

		override public bool test() {

			SumNEntiers cp = new SumNEntiers ();

			cp.N = 42;

			if (cp.N == 42) {

				return true;

			} else {

				return false;
			}

		}

	}

}